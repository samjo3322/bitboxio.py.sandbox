import tkinter as tk
from tkinter import ttk

def treeview_sort_column(tv, col, reverse):
    print('sorting %s!' % col)
    l = [(tv.set(k, col), k) for k in tv.get_children('')]
    l.sort(reverse=reverse)

    # rearrange items in sorted positions
    for index, (val, k) in enumerate(l):
        print('Moving Index:%r, Value:%r, k:%r' % (index, val, k))
        tv.move(k, '', index)

    # reverse sort next time
    tv.heading(col, command=lambda: treeview_sort_column(tv, col, not reverse))

cols = ('name', 'path', 'time', 'pb')

root = tk.Tk()
root.geometry("700x500")
listbox = ttk.Treeview(root, columns=cols, show="headings")
for each in ('name', 'path', 'time','pb'):
    listbox.heading(each,text=each.capitalize(),command=lambda: treeview_sort_column(listbox, each, False))
    listbox.column( each, width='100' )
    if not each == 'path':
        listbox.column(each,stretch=False)
    if not each == 'name':
        listbox.column( each, anchor='center')

listbox.pack(expand=True, fill=tk.BOTH)
listbox.insert('', 'end', text="First", values=('10:00','10:10', 'Ok', 'First'))
listbox.insert('', 'end', text="Second", values=('10:00','10:10', 'Ok', 'Second'))
listbox.insert('', 'end', text="Second", values=('10:00','10:10', 'Ok', 'Third'))

root.mainloop()


'''
pyinstaller --windowed myapp.py
cd dist/myapp.app/Contents/MacOs
mkdir tcl tk
cp -R /Library/Frameworks/Python.framework/Versions/3.7/lib/tcl* tcl/
cp -R /Library/Frameworks/Python.framework/Versions/3.7/lib/tk* tk/
cp -R /Library/Frameworks/Python.framework/Versions/3.7/lib/Tk* tk/ 
'''
import random
import card

class Deck():

    def __init__(self):
        self.cards = []
        self.suits = ('Hearts', 'Diamonds', 'Spades', 'Clubs')
        self.ranks = ('Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine', 'Ten', 'Jack', 'Queen', 'King', 'Ace')
        self.value = {'Two':2, 'Three':3, 'Four':4, 'Five':5, 'Six':6, 'Seven':7, 'Eight':8, 'Nine':9, 'Ten':10, 'Jack':10, 'Queen':10, 'King':10, 'Ace':11}

        for suit in self.suits:
            cardsuit = suit
            for rank in self.ranks:
                cardrank = rank
                cardvalue = self.value[cardrank]
                self.cards.append(card.Card(cardsuit,cardrank,cardvalue))

    def deal(self):
        single_card = self.cards.pop()
        return single_card

    def __str__(self):
        deck_comp = ''  # start with an empty string
        for card in self.cards:
            deck_comp += '\n '+card.__str__() # add each Card object's print string
        return 'The deck has:' + deck_comp

    def shuffle(self):
        random.shuffle(self.cards)

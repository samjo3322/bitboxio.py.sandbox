

n = int(input('Please enter a whole number greater than 1:\n'))

def collatz(n):

    if n > 1:
        print('Start counting steps.')
        i = 0
        while n > 1:
            # If *n* is even, divide it by 2.
            # If *n* is odd, multiply it by 3 and add 1
            if n%2 == 0:
                # even
                n = n / 2
            else:
                # odd
                n = (n * 3) + 1

            i += 1
            print(f'n is {n}')
            #break
    else:
        print('Please enter a whole number greater than 1')

    print(f' n = {n}, it took {i} steps')

collatz(n)